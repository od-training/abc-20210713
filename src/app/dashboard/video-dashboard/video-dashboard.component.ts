import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';

import { Video } from 'src/app/app-types';
import { VideoDataService } from 'src/app/video-data.service';
import { filter, map, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-video-dashboard',
  templateUrl: './video-dashboard.component.html',
  styleUrls: ['./video-dashboard.component.scss'],
})
export class VideoDashboardComponent implements OnInit {
  videos: Observable<Video[]>;
  selectedVideo: Observable<Video>;

  constructor(videoSvc: VideoDataService, route: ActivatedRoute) {
    this.videos = videoSvc.videoList;

    this.selectedVideo = route.queryParamMap.pipe(
      map((params) => params.get('videoId') as string),
      filter((id) => !!id),
      switchMap((id) => videoSvc.loadSingleVideo(id))
    );
  }

  ngOnInit(): void {}
}
